# Hyperborea

## About
Hyperborea is an intermediate library between the Asphodel Python Library and high level applications.

Hyperborea provides process isolation, streaming, disk recording and uploading functionality.

The Asphodel communication protocol was developed by Suprock Technologies (http://www.suprocktech.com)

## License
Hyperborea is licensed under the ISC license.

The ISC license is a streamlined version of the BSD license, and permits usage in both open source and propretary projects.
